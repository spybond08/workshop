module "vpc" {
  source = "../modules/vpc"
  cidr = ["10.202.0.0/16"]
  env = var.env
  zone = var.zone
  folder_id = var.folder_id
}