terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
      version = "0.57.0"
    }
  }
}

resource "yandex_vpc_network" "internal" {
  name = "${var.env}-vpc"
}

resource "yandex_vpc_subnet" "internal-c" {
  name       = "${var.env}-subnet"
  zone = var.zone
  network_id = yandex_vpc_network.internal.id
  v4_cidr_blocks = var.cidr
  folder_id = var.folder_id
}